DECLARE @Proc VARCHAR(max);
IF OBJECT_ID('WarehouseV','V') IS NULL
BEGIN
	SET @Proc = '
	CREATE VIEW WarehouseV(City, Street, LocalNumber, MaxCapacity, ActualCapacity)
	AS (
		SELECT T1.City, T1.Street, T1.LocalNumber, T1.MaxCapacity, T1.ActualCapacity
		FROM Warehouse T1
	);
	'
	EXEC(@Proc);
END

IF OBJECT_ID('ProductsV','V') IS NULL
BEGIN
	SET @Proc = '
		CREATE VIEW ProductsV(Name, UnitPrice)
		AS (
		    SELECT T1.Name,T1.UnitPrice
		    FROM Products T1
		);
		'
	EXEC(@Proc);
END

IF OBJECT_ID('ProductsV','V') IS NULL
BEGIN
	SET @Proc = '
		CREATE VIEW WarehouseInvetV(City, Streat, LocalNumber, Product, Qty)
		AS (
		    SELECT T2.City, T2.Street, T2.LocalNumber, T3.Name, T1.Qty
		    FROM WareHouseInvent T1
				INNER JOIN Warehouse T2
				ON T1.WarehouseID = T2.WarehouseID

				INNER JOIN Products T3
				ON T1.ProductsID = T3.ProductsID
		);
		'
		EXEC(@Proc)
END

IF OBJECT_ID('ProductsV','V') IS NULL
BEGIN
	SET @Proc = '
		CREATE VIEW SalesLineV(ID, City, Streat, LocalNumber, Product, Qty, Value)
		AS (
		    SELECT T1.SalesLineID, T2.City, T2.Street, T2.LocalNumber, T3.Name, T1.Qty, T1.Value
		    FROM SalesLine T1
				INNER JOIN Warehouse T2
				ON T1.WarehouseID = T2.WarehouseID

				INNER JOIN Products T3
				ON T1.ProductsID = T3.ProductsID
		);
		'
	EXEC(@Proc)
END
IF OBJECT_ID('ProductsV','V') IS NULL
BEGIN
	SET @Proc = '
		CREATE VIEW WarehouseManV(Name, Surname, BirthDate, Wage, Boss, WarehouseCity, WarehouseStreet, WarehouseLocalNumber)
		AS (
		    SELECT T1.Name, T1.Surname, T1.BirthDate, T1.Wage, T2.Surname, T3.City, T3.Street, T3.LocalNumber
		    FROM WarehouseMan T1
				INNER JOIN WarehouseMan T2
				ON T1.BossID = T2.WarehouseManID

				INNER JOIN Warehouse T3
				ON T1.WarehouseID = T3.WarehouseID
		);
		'
	EXEC(@Proc)
END
IF OBJECT_ID('ProductsV','V') IS NULL
BEGIN
	SET @Proc = '
		CREATE VIEW SalesV(City,Streat,LocalNumber,ShipmentStatus, WarehouseMan)
		AS (
		    SELECT T1.City, T1.Street, T1.LocalNumber, T1.ShipmentStatus, T2.Surname
		    FROM Sales T1
				INNER JOIN WarehouseMan T2
				ON T1.WarehouseManID = T2.WarehouseManID
		);
		'
	EXEC(@Proc)
END

