DECLARE @Proc VARCHAR(max);
IF OBJECT_ID('CreateSale','P') IS NULL
BEGIN
	SET @Proc = '
	CREATE PROCEDURE CreateSale
	    @City			VARCHAR(100),
		@ZipCode		VARCHAR(100),
		@Street			VARCHAR(100),
		@LocalNumber	VARCHAR(100),
		@WarehouseMan	INT
	AS
		DECLARE @Output INT;
		BEGIN TRANSACTION
			INSERT INTO [Sales] (City,ZipCode,Street,LocalNumber,WarehouseManID) VALUES
				(@Street,@ZipCode,@Street,@LocalNumber,@WarehouseMan);
		COMMIT
		SET @Output = (SELECT T1.SalesID FROM [Sales] T1 WHERE T1.SalesID >= ALL(SELECT T2.SalesID FROM Sales T2))
		PRINT ('''+'Created new sales with ID:'''+'+@Output)
	'
	EXEC(@Proc);
	PRINT('Procedure CreateSale[P] created.')
END
ELSE
BEGIN	
	PRINT('Procedure CreateSale[P] creation faild.')
END

IF OBJECT_ID('UpdateAllSalesLineValue','P') IS NULL
BEGIN
	SET @Proc = '
		CREATE PROCEDURE UpdateAllSalesLineValue
		AS
			BEGIN TRANSACTION
				UPDATE SalesLine
				SET Value = Qty * (SELECT T1.UnitPrice FROM Products T1 WHERE T1.ProductsID = SalesLine.ProductsID)
			COMMIT
	'
	EXEC(@Proc);
	PRINT('Procedure UpdateAllSalesLineValue[P] created.')
END
ELSE
BEGIN	
	PRINT('Procedure UpdateAllSalesLineValue[P] creation faild.')
END

IF OBJECT_ID('ReserveProduct','P') IS NULL
BEGIN
	SET @Proc = '
		CREATE PROCEDURE ReserveProduct
			@WarehouseId int,
			@ProductId int,
			@Qty int
		AS
			UPDATE WarehouseInvent
			SET Qty = Qty - @Qty
			WHERE WarehouseID = @WarehouseId
				AND ProductsID = @ProductId
	'
	EXEC(@Proc);
	PRINT('Procedure ReserveProduct[P] created.')
END
ELSE
BEGIN	
	PRINT('Procedure ReserveProduct[P] creation faild.')
END

IF OBJECT_ID('AddSalesLine','P') IS NULL
BEGIN
	SET @Proc = '
		CREATE PROCEDURE AddSalesLine
			@SalesId INT,
			@ProductId INT,
			@Qty INT,
			@WarehouseId INT
		AS
			DECLARE @SalesLineId INT,@RC INT;
			SET @SalesLineId = dbo.GetSalesLine(@ProductId,@SalesId);
			IF @SalesLineId IS NULL
			BEGIN
				BEGIN TRANSACTION UpdatingSalesLine
					IF(dbo.CanReserve(@ProductId,@SalesLineId,@Qty)=1)
					BEGIN
						UPDATE SalesLine SET Qty = @Qty WHERE SalesLineID = @SalesLineId
						EXECUTE @RC = [dbo].[ReserveProduct] @WarehouseId,@ProductId,@Qty
						EXECUTE @RC = [dbo].[UpdateAllSalesLineValue]
					END
					ELSE
					BEGIN
						ROLLBACK
					END
				COMMIT
			END
	'
	EXEC(@Proc);
	PRINT('Procedure AddSalesLine[P] created.')
END
ELSE
BEGIN	
	PRINT('Procedure AddSalesLine[P] creation faild.')
END

IF OBJECT_ID('AddProductToWarehouse','P') IS NULL
BEGIN
	SET @Proc = '
	CREATE PROCEDURE AddProductToWarehouse
	@WarehouseId int,
	@ProductId int,
	@Qty int
	AS
	DECLARE @RC INT;

	IF(dbo.IsPlaceInWarehouse(@WarehouseId,@Qty)=1)
	BEGIN
		UPDATE Warehouse
		SET ActualCapacity = ActualCapacity + @Qty
		WHERE WarehouseID = @WarehouseId

		SET @Qty = @Qty * -1
		EXECUTE @RC = [dbo].[ReserveProduct] @WarehouseId,@ProductId,@Qty
	END
	'
	EXEC(@Proc);
	PRINT('Procedure AddProductToWarehouse[P] created.')
END
ELSE
BEGIN	
	PRINT('Procedure AddProductToWarehouse[P] creation faild.')
END

IF OBJECT_ID('ChangeShipStatus','P') IS NULL
BEGIN
	SET @Proc = '
	CREATE PROCEDURE ChangeShipStatus
	@SlaesId INT
	AS
	DECLARE @ShipmentList AS TABLE(
		WarehouseId int,
		Qty int
	)

	BEGIN TRANSACTION
		INSERT INTO @ShipmentList 
			SELECT T1.WarehouseID, SUM(T1.Qty)
			FROM SalesLine T1
			WHERE T1.SalesLineID = ANY(SELECT T2.SalesLineID FROM SalesTrans T2 WHERE T2.SalesID = @SlaesId)
			GROUP BY (T1.WarehouseID)

		UPDATE T1
		SET T1.ActualCapacity = T1.ActualCapacity - T2.Qty
		FROM Warehouse T1, @ShipmentList T2
		WHERE T1.WarehouseID = T2.WarehouseId

		UPDATE Sales
		SET ShipmentStatus = '''+'Z'+'''
		WHERE SalesID = @SlaesId
	COMMIT
	'
	EXEC(@Proc);
	PRINT('Procedure ChangeShipStatus[P] created.')
END
ELSE
BEGIN	
	PRINT('Procedure ChangeShipStatus[P] creation faild.')
END

IF OBJECT_ID('DeleteSalesLine','P') IS NULL
BEGIN
	SET @Proc = '
		CREATE PROCEDURE DeleteSalesLine
			@SalesId INT,
			@ProductId INT,
			@WarehouseId INT
		AS
			DECLARE @SalesLineId INT,@RC INT;
			SET @SalesLineId = dbo.GetSalesLine(@ProductId,@SalesId);
			IF @SalesLineId IS NOT NULL
			BEGIN
				BEGIN TRANSACTION DeleteSalesLine
					DELETE FROM SalesTrans WHERE SalesLineID = @SalesLineId
					DELETE FROM SalesLine WHERE SalesLineID = @SalesLineId
				COMMIT
			END
	'
	EXEC(@Proc);
	PRINT('Procedure DeleteSalesLine[P] created.')
END
ELSE
BEGIN	
	PRINT('Procedure DeleteSalesLine[P] creation faild.')
END