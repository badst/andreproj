SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* Cialo finkcji */
IF OBJECT_ID('Warehouse', 'U') IS NOT NULL
BEGIN
	DECLARE @WareHouseInsertTr AS varchar(max),
			@WareHouseUpdateTr AS varchar(max)

	SET @WareHouseInsertTr = '
	CREATE TRIGGER WarehouseInsert
		ON  [Warehouse]
		INSTEAD OF INSERT
		AS
		BEGIN
			DECLARE @InsertTab AS TABLE(
				WarehouseID INT,
				MaxCapacity INT,
				ActualCapacity INT,
				City VARCHAR(100),
				ZipCode VARCHAR(100),
				Street VARCHAR(100),
				LocalNumber VARCHAR(100),
				CreationUser VARCHAR(100),
				CreationDate DATE,
				ModificationUser VARCHAR(100),
				ModificationDate DATE
			)

			INSERT INTO @InsertTab SELECT * FROM inserted;

			UPDATE @InsertTab
				SET CreationDate = GETDATE(),
					CreationUser = (SELECT CURRENT_USER)
			;
			
			INSERT INTO Warehouse SELECT T1.MaxCapacity,T1.ActualCapacity,T1.City,T1.ZipCode,T1.Street,T1.LocalNumber,T1.CreationUser,
				T1.CreationDate,T1.ModificationUser,T1.ModificationDate FROM @InsertTab T1
		END
	'
	SET @WareHouseUpdateTr = '
	CREATE TRIGGER WarehouseUpdate
		ON  [Warehouse] 
		AFTER UPDATE
		AS
		BEGIN
			UPDATE [Warehouse]
				SET ModificationDate = GETDATE(),
					ModificationUser = (SELECT CURRENT_USER)
				WHERE [WareHouse].WarehouseID = ANY (
					SELECT WarehouseID FROM inserted
				);
		END
		'
	PRINT('Starting triggers creation for Warehouse[T]')

	IF OBJECT_ID('WarehouseInsert', 'TR') IS NULL
	BEGIN
		EXEC(@WareHouseInsertTr);
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of WarehouseInsert[Tr] faild WarehouseInsert[Tr] already exist')

	IF OBJECT_ID('WareHouseUpdate', 'TR') IS NULL
	BEGIN
		EXEC(@WareHouseUpdateTr)
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of WarehouseUpdate[Tr] faild WarehouseUpdate[Tr] already exist')
END
ELSE
BEGIN
	PRINT('Creation of triggers for WareHouse[T] faild.Table not exist')
END

IF OBJECT_ID('Sales', 'U') IS NOT NULL
BEGIN
	DECLARE @SalesInsertTr AS varchar(max),
			@SalesUpdateTr AS varchar(max)

	SET @SalesInsertTr = '
	CREATE TRIGGER SalesInsert
		ON  [Sales]
		INSTEAD OF INSERT
		AS
		BEGIN
			DECLARE @InsertTab AS TABLE(
				SalesID INT,
				ShipmentStatus CHAR(1),
				City VARCHAR(100),
				ZipCode VARCHAR(100),
				Street VARCHAR(100),
				LocalNumber VARCHAR(100),
				CreationUser VARCHAR(100),
				CreationDate DATE,
				ModificationUser VARCHAR(100),
				ModificationDate DATE,
				WarehouseManID INT
			)

			INSERT INTO @InsertTab SELECT * FROM inserted;

			UPDATE @InsertTab
				SET CreationDate = GETDATE(),
					CreationUser = (SELECT CURRENT_USER)
			;

			INSERT INTO Sales SELECT T1.ShipmentStatus,T1.City,T1.ZipCode,T1.Street,T1.LocalNumber,T1.CreationUser,
				T1.CreationDate,T1.ModificationUser,T1.ModificationDate,T1.WarehouseManID FROM @InsertTab T1
		END
	'
	SET @SalesUpdateTr = '
	CREATE TRIGGER SalesUpdate
		ON  [Sales] 
		AFTER UPDATE
		AS
		BEGIN
			UPDATE [Sales]
				SET ModificationDate = GETDATE(),
					ModificationUser = (SELECT CURRENT_USER)
				WHERE [Sales].SalesID = ANY (
					SELECT SalesID FROM inserted
				);
		END
		'
	PRINT('Starting triggers creation for Sales[T]')

	IF OBJECT_ID('SalesInsert', 'TR') IS NULL
	BEGIN
		EXEC(@SalesInsertTr);
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of SalesInsert[Tr] faild SalesInsert[Tr] already exist')

	IF OBJECT_ID('SalesUpdate', 'TR') IS NULL
	BEGIN
		EXEC(@SalesUpdateTr)
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of SalesUpdate[Tr] faild SalesUpdate[Tr] already exist')
END
ELSE
BEGIN
	PRINT('Creation of triggers for Sales[T] faild.Table not exist')
END

IF OBJECT_ID('Products', 'U') IS NOT NULL
BEGIN
	DECLARE @ProductsInsertTr AS varchar(max),
			@ProductsUpdateTr AS varchar(max)

	SET @ProductsInsertTr = '
	CREATE TRIGGER ProductsInsert
		ON  [Products]
		INSTEAD OF INSERT
		AS
		BEGIN
			DECLARE @InsertTab AS TABLE(
				ProductsID INT,
				Name VARCHAR(100),
				UnitPrice FLOAT,
				CreationUser VARCHAR(100),
				CreationDate DATE,
				ModificationUser VARCHAR(100),
				ModificationDate DATE
			)

			INSERT INTO @InsertTab SELECT * FROM inserted;

			UPDATE @InsertTab
				SET CreationDate = GETDATE(),
					CreationUser = (SELECT CURRENT_USER)
			;

			INSERT INTO Products SELECT T1.Name,T1.UnitPrice,T1.CreationUser,T1.CreationDate,T1.ModificationUser,T1.ModificationDate FROM @InsertTab T1
		END
	'
	SET @ProductsUpdateTr = '
	CREATE TRIGGER ProductsUpdate
		ON  [Products] 
		AFTER UPDATE
		AS
		BEGIN
			UPDATE [Products]
				SET ModificationDate = GETDATE(),
					ModificationUser = (SELECT CURRENT_USER)
				WHERE [Products].ProductsID = ANY (
					SELECT ProductsID FROM inserted
				);
		END
		'
	PRINT('Starting triggers creation for Products[T]')

	IF OBJECT_ID('ProductsInsert', 'TR') IS NULL
	BEGIN
		EXEC(@ProductsInsertTr);
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of ProductsInsert[Tr] faild ProductsInsert[Tr] already exist')

	IF OBJECT_ID('ProductsUpdate', 'TR') IS NULL
	BEGIN
		EXEC(@ProductsUpdateTr)
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of ProductsUpdate[Tr] faild ProductsUpdate[Tr] already exist')
END
ELSE
BEGIN
	PRINT('Creation of triggers for Products[T] faild.Table not exist')
END

IF OBJECT_ID('WarehouseInvent', 'U') IS NOT NULL
BEGIN
	DECLARE @WareHouseInventInsertTr AS varchar(max),
			@WareHouseInventUpdateTr AS varchar(max)

	SET @WareHouseInventInsertTr = '
	CREATE TRIGGER WarehouseInventInsert
		ON  [WarehouseInvent]
		INSTEAD OF INSERT
		AS
		BEGIN
			DECLARE @InsertTab AS TABLE(
				WarehouseID INT,
				ProductsID INT,
				Qty INT,
				CreationUser VARCHAR(100),
				CreationDate DATE,
				ModificationUser VARCHAR(100),
				ModificationDate DATE
			)

			INSERT INTO @InsertTab SELECT * FROM inserted;

			UPDATE @InsertTab
				SET CreationDate = GETDATE(),
					CreationUser = (SELECT CURRENT_USER)
			;

			INSERT INTO WarehouseInvent SELECT * FROM @InsertTab
		END
	'
	SET @WareHouseInventUpdateTr = '
	CREATE TRIGGER WarehouseInventUpdate
		ON  [WarehouseInvent] 
		AFTER UPDATE
		AS
		BEGIN
			UPDATE [WarehouseInvent]
				SET ModificationDate = GETDATE(),
					ModificationUser = (SELECT CURRENT_USER)
				WHERE [WarehouseInvent].WarehouseID = ANY (
					SELECT WarehouseID FROM inserted
				) AND [WarehouseInvent].ProductsID = ANY (
					SELECT ProductsID FROM inserted
				)
		END
		'
	PRINT('Starting triggers creation for WarehouseInvent[T]')

	IF OBJECT_ID('WarehouseInventInsert', 'TR') IS NULL
	BEGIN
		EXEC(@WarehouseInventInsertTr);
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of WarehouseInventInsert[Tr] faild WarehouseInventInsert[Tr] already exist')

	IF OBJECT_ID('WarehouseInventUpdate', 'TR') IS NULL /* Change char for triggers */
	BEGIN
		EXEC(@WareHouseInventUpdateTr)
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of WareHouseInventUpdate[Tr] faild WareHouseInventUpdate[Tr] already exist')
END
ELSE
BEGIN
	PRINT('Creation of triggers for WareHouseInvent[T] faild.Table not exist')
END

IF OBJECT_ID('WarehouseMan', 'U') IS NOT NULL
BEGIN
	DECLARE @WarehouseManInsertTr AS varchar(max),
			@WarehouseManUpdateTr AS varchar(max)

	SET @WarehouseManInsertTr = '
	CREATE TRIGGER WarehouseManInsert
		ON  [dbo].[WarehouseMan]
		INSTEAD OF INSERT
		AS
		BEGIN
			DECLARE @InsertTab AS TABLE(
				WarehouseManID INT,
				Name VARCHAR(100),
				Surname VARCHAR(100),
				BirthDate DATE,
				Wage INT,
				CreationUser VARCHAR(100),
				CreationDate DATE,
				ModificationUser VARCHAR(100),
				ModificationDate DATE,
				BossID INT,
				WarehouseID INT
			)

			INSERT INTO @InsertTab SELECT * FROM inserted;

			UPDATE @InsertTab
				SET CreationDate = GETDATE(),
					CreationUser = (SELECT CURRENT_USER)
			;

			INSERT INTO WarehouseMan SELECT T1.Name,T1.Surname,T1.BirthDate,T1.Wage,T1.CreationUser,T1.CreationDate,T1.ModificationUser,T1.ModificationDate,
				T1.BossID,T1.WarehouseID FROM @InsertTab T1
		END
	'
	SET @WarehouseManUpdateTr = '
	CREATE TRIGGER WarehouseManUpdate
		ON  [WarehouseMan] 
		AFTER UPDATE
		AS
		BEGIN
			UPDATE [WarehouseMan]
				SET ModificationDate = GETDATE(),
					ModificationUser = (SELECT CURRENT_USER)
				WHERE [WarehouseMan].WarehouseManID = ANY (
					SELECT WarehouseManID FROM inserted
				)
		END
		'
	PRINT('Starting triggers creation for WarehouseMan[T]')

	IF OBJECT_ID('WarehouseManInsert', 'TR') IS NULL
	BEGIN
		EXEC(@WarehouseManInsertTr);
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of WarehouseManInsert[Tr] faild WarehouseManInsert[Tr] already exist')

	IF OBJECT_ID('WarehouseManUpdate', 'TR') IS NULL
	BEGIN
		EXEC(@WarehouseManUpdateTr)
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of WarehouseManUpdate[Tr] faild WarehouseManUpdate[Tr] already exist')
END
ELSE
BEGIN
	PRINT('Creation of triggers for WarehouseManInvent[T] faild.Table not exist')
END

IF OBJECT_ID('SalesLine', 'U') IS NOT NULL
BEGIN
	DECLARE @SalesLineInsertTr AS varchar(max),
			@SalesLineUpdateTr AS varchar(max)

	SET @SalesLineInsertTr = '
	CREATE TRIGGER SalesLineInsert
		ON  [SalesLine]
		INSTEAD OF INSERT
		AS
		BEGIN
			DECLARE @InsertTab AS TABLE(
				SalesLineID INT,
				WarehouseID INT,
				ProductsID INT,
				Qty INT,
				Value FLOAT,
				CreationUser VARCHAR(100),
				CreationDate DATE,
				ModificationUser VARCHAR(100),
				ModificationDate DATE
			)

			INSERT INTO @InsertTab SELECT * FROM inserted;

			UPDATE @InsertTab
				SET CreationDate = GETDATE(),
					CreationUser = (SELECT CURRENT_USER)
			;

			INSERT INTO SalesLine SELECT T1.WarehouseID,T1.ProductsID,T1.Qty,T1.Value,T1.CreationUser,T1.CreationDate,
				T1.ModificationUser,T1.ModificationDate FROM @InsertTab T1
		END
	'
	SET @SalesLineUpdateTr = '
	CREATE TRIGGER SalesLineUpdate
		ON  [SalesLine] 
		AFTER UPDATE
		AS
		BEGIN
			UPDATE [SalesLine]
				SET ModificationDate = GETDATE(),
					ModificationUser = (SELECT CURRENT_USER)
				WHERE [SalesLine].SalesLineID = ANY (
					SELECT SalesLineID FROM inserted
				)
		END
		'
	PRINT('Starting triggers creation for SalesLine[T]')

	IF OBJECT_ID('SalesLineInsert', 'TR') IS NULL
	BEGIN
		EXEC(@SalesLineInsertTr);
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of SalesLineInsert[Tr] faild SalesLineInsert[Tr] already exist')

	IF OBJECT_ID('SalesLineUpdate', 'TR') IS NULL
	BEGIN
		EXEC(@SalesLineUpdateTr)
		PRINT('Trigger created succesfully')
	END
	ELSE PRINT('Creation of SalesLineUpdate[Tr] faild SalesLineUpdate[Tr] already exist')
END
ELSE
BEGIN
	PRINT('Creation of triggers for SalesLineInvent[T] faild.Table not exist')
END
GO