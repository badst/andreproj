/* Tabele */
BEGIN TRANSACTION
	IF OBJECT_ID('SalesTrans', 'U') IS NOT NULL 
		DROP TABLE SalesTrans;
	IF OBJECT_ID('Sales', 'U') IS NOT NULL 
		DROP TABLE Sales;
	IF OBJECT_ID('WarehouseMan', 'U') IS NOT NULL 
		DROP TABLE WarehouseMan;
	IF OBJECT_ID('SalesLine', 'U') IS NOT NULL 
		DROP TABLE SalesLine;
	IF OBJECT_ID('WareHouseInvent', 'U') IS NOT NULL 
		DROP TABLE WareHouseInvent;
	IF OBJECT_ID('Products', 'U') IS NOT NULL 
		DROP TABLE Products;
	IF OBJECT_ID('WareHouse', 'U') IS NOT NULL 
		DROP TABLE WareHouse;
COMMIT
/* Vievs */
BEGIN TRANSACTION
	IF OBJECT_ID('ProductsV','V') IS NOT NULL
		DROP VIEW ProductsV;
	IF OBJECT_ID('SalesLineV','V') IS NOT NULL
		DROP VIEW SalesLineV;
	IF OBJECT_ID('SalesV','V') IS NOT NULL
		DROP VIEW SalesV;
	IF OBJECT_ID('WarehouseInvetV','V') IS NOT NULL
		DROP VIEW WarehouseInvetV;
	IF OBJECT_ID('WarehouseManV','V') IS NOT NULL
		DROP VIEW WarehouseManV;
	IF OBJECT_ID('WarehouseV','V') IS NOT NULL
		DROP VIEW WarehouseV;
COMMIT
/* Functions */
BEGIN TRANSACTION
	IF OBJECT_ID('GetSalesLine','FN') IS NOT NULL
		DROP FUNCTION dbo.GetSalesLine
	IF OBJECT_ID('CanReserve','FN') IS NOT NULL
		DROP FUNCTION dbo.CanReserve
	IF OBJECT_ID('IsPlaceInWarehouse','FN') IS NOT NULL
		DROP FUNCTION dbo.IsPlaceInWarehouse
/* Table Function */
	IF OBJECT_ID('SalesRaport','TF') IS NOT NULL
		DROP FUNCTION dbo.SalesRaport
COMMIT
/* Procedures */
BEGIN TRANSACTION
	IF OBJECT_ID('CreateSale','P') IS NOT NULL
		DROP PROCEDURE [dbo].[CreateSale]
	IF OBJECT_ID('UpdateAllSalesLineValue','P') IS NOT NULL
		DROP PROCEDURE [dbo].UpdateAllSalesLineValue
	IF OBJECT_ID('ReserveProduct','P') IS NOT NULL
		DROP PROCEDURE [dbo].ReserveProduct
	IF OBJECT_ID('AddSalesLine','P') IS NOT NULL
		DROP PROCEDURE [dbo].AddSalesLine
	IF OBJECT_ID('AddProductToWarehouse','P') IS NOT NULL
		DROP PROCEDURE [dbo].AddProductToWarehouse
	IF OBJECT_ID('ChangeShipStatus','P') IS NOT NULL
		DROP PROCEDURE [dbo].ChangeShipStatus
	IF OBJECT_ID('DeleteSalesLine','P') IS NOT NULL
		DROP PROCEDURE [dbo].DeleteSalesLine
COMMIT