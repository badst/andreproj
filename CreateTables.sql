IF OBJECT_ID('Warehouse', 'U') IS NULL
BEGIN
	CREATE TABLE Warehouse(
		WarehouseID INT NOT NULL IDENTITY(1,1),
		MaxCapacity INT NOT NULL,
		ActualCapacity INT NOT NULL DEFAULT(0),
		City VARCHAR(100) NOT NULL,
		ZipCode VARCHAR(100) NOT NULL,
		Street VARCHAR(100) NOT NULL,
		LocalNumber VARCHAR(100) NOT NULL,
		CreationUser VARCHAR(100),
		CreationDate DATE,
		ModificationUser VARCHAR(100),
		ModificationDate DATE,
		PRIMARY KEY (WarehouseID),
		UNIQUE (City, ZipCode, Street, LocalNumber)
	)
	PRINT('Created Warehouse[Table]')
END
ELSE
BEGIN
	PRINT('Creation of Warehouse[Table] faild. Warehouse[Table] already exist')
END
GO
IF OBJECT_ID('Products', 'U') IS NULL
BEGIN
	CREATE TABLE Products(
		ProductsID INT NOT NULL IDENTITY(1,1),
		Name VARCHAR(100) NOT NULL,
		UnitPrice FLOAT NOT NULL DEFAULT(0),
		CreationUser VARCHAR(100),
		CreationDate DATE,
		ModificationUser VARCHAR(100),
		ModificationDate DATE,
		PRIMARY KEY (ProductsID),
		UNIQUE (Name)
	);
	PRINT('Created Products[Table]')
END
ELSE
BEGIN
	PRINT('Creation of Products[Table] faild Products[Table] already exist')
END
GO
IF OBJECT_ID('WarehouseInvent', 'U') IS NULL
BEGIN
	CREATE TABLE WarehouseInvent(
		WarehouseID INT NOT NULL,
		ProductsID INT NOT NULL,
		Qty INT NOT NULL DEFAULT(0),
		CreationUser VARCHAR(100),
		CreationDate DATE,
		ModificationUser VARCHAR(100),
		ModificationDate DATE,
		PRIMARY KEY (WarehouseID, ProductsID),
		FOREIGN KEY (WarehouseID) REFERENCES Warehouse(WarehouseID),
		FOREIGN KEY (ProductsID) REFERENCES Products(ProductsID)
	);
	PRINT('Created WarehouseInvent[Table]')
END
ELSE
BEGIN
	PRINT('Creation of WarehouseInvent[Table] faild WarehouseInvent[Table] already exist')
END
GO
IF OBJECT_ID('SalesLine', 'U') IS NULL
BEGIN
	CREATE TABLE SalesLine(
		SalesLineID INT NOT NULL IDENTITY(1,1),
		WarehouseID INT NOT NULL,
		ProductsID INT NOT NULL,
		Qty INT NOT NULL DEFAULT(0),
		Value FLOAT NOT NULL DEFAULT(0),
		CreationUser VARCHAR(100),
		CreationDate DATE,
		ModificationUser VARCHAR(100),
		ModificationDate DATE,
		PRIMARY KEY (SalesLineID),
		FOREIGN KEY (WarehouseID) REFERENCES WareHouse(WarehouseID),
		FOREIGN KEY (ProductsID) REFERENCES Products(ProductsID)
	);
	PRINT('Created SalesLine[Table]')
END
ELSE
BEGIN
	PRINT('Creation of SalesLine[Table] faild SalesLine[Table] already exist')
END
GO
IF OBJECT_ID('WarehouseMan', 'U') IS NULL
BEGIN
	CREATE TABLE WarehouseMan(
		WarehouseManID INT NOT NULL IDENTITY(1,1),
		Name VARCHAR(100) NOT NULL,
		Surname VARCHAR(100) NOT NULL,
		BirthDate DATE,
		Wage INT DEFAULT(0),
		CreationUser VARCHAR(100),
		CreationDate DATE,
		ModificationUser VARCHAR(100),
		ModificationDate DATE,
		BossID INT,
		WarehouseID INT NOT NULL,
		PRIMARY KEY (WarehouseManID),
		FOREIGN KEY (BossID) REFERENCES WarehouseMan(WarehouseManID),
		FOREIGN KEY (WarehouseID) REFERENCES Warehouse(WarehouseID)
	);
	PRINT('Created WarehouseMan[Table]')
END
ELSE
BEGIN
	PRINT('Creation of WarehouseMan[Table] faild WarehouseMan[Table] already exist')
END
GO
IF OBJECT_ID('Sales', 'U') IS NULL
BEGIN
	CREATE TABLE Sales(
		SalesID INT NOT NULL IDENTITY(1,1),
		ShipmentStatus CHAR(1) NOT NULL DEFAULT('O'),
		City VARCHAR(100) NOT NULL,
		ZipCode VARCHAR(100) NOT NULL,
		Street VARCHAR(100) NOT NULL,
		LocalNumber VARCHAR(100) NOT NULL,
		CreationUser VARCHAR(100),
		CreationDate DATE,
		ModificationUser VARCHAR(100),
		ModificationDate DATE,
		WarehouseManID INT NOT NULL,
		PRIMARY KEY (SalesID),
		FOREIGN KEY (WarehouseManID) REFERENCES WarehouseMan(WarehouseManID),
		CHECK (ShipmentStatus = 'O' OR ShipmentStatus = 'Z'),
		UNIQUE (City, ZipCode, Street, LocalNumber)
	);
	PRINT('Created Sales[Table]')
END
ELSE
BEGIN
	PRINT('Creation of Sales[Table] faild Sales[Table] already exist')
END
GO
IF OBJECT_ID('SalesTrans', 'U') IS NULL
BEGIN
	CREATE TABLE SalesTrans(
		SalesLineID INT NOT NULL,
		SalesID INT NOT NULL,
		PRIMARY KEY (SalesLineID, SalesID),
		FOREIGN KEY (SalesLineID) REFERENCES SalesLine(SalesLineID),
		FOREIGN KEY (SalesID) REFERENCES Sales(SalesID)
	);
	PRINT('Created SalesTrans[Table]')
END
ELSE
BEGIN
	PRINT('Creation of SalesTrans[Table] faild SalesTrans[Table] already exist')
END
GO