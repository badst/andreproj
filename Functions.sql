DECLARE @Func AS varchar(max)

IF OBJECT_ID('GetSalesLine','FN') IS NULL
BEGIN
	SET @Func = '
	CREATE FUNCTION GetSalesLine (
		@ItemId int,
		@SalesId int)
		RETURNS INT
	AS
	BEGIN
		DECLARE @Return int
		DECLARE @SalesTransTemp AS TABLE(
			SalesLineID int,
			SalesID int
			)
	
		INSERT INTO @SalesTransTemp SELECT * FROM [SalesTrans] T1 WHERE T1.SalesID = @SalesId;

		SET @Return = (
			SELECT T1.SalesLineID FROM SalesLine T1 
			WHERE T1.SalesLineID = ANY (SELECT T2.SalesLineID FROM @SalesTransTemp T2)
				AND T1.ProductsID = @ItemId
			)

		RETURN @Return
	END
	'
	EXEC(@Func);
	PRINT('Function GetSalesLine[FN] created.')
END
ELSE
BEGIN
	PRINT('Function GetSalesLine[FN] creation faild.')
END

IF OBJECT_ID('CanReserve','FN') IS NULL
BEGIN
	SET @Func = '
		CREATE FUNCTION CanReserve (
			@ItemId int,
			@SalesId int,
			@Qty int
			)
			RETURNS int
		AS
		BEGIN
			DECLARE @Return int,@SalesLine int;
			SET @SalesLine = dbo.GetSalesLine(@ItemId,@SalesId);
		
			IF(
				(SELECT T1.Qty FROM WarehouseInvent T1 
					WHERE T1.WarehouseID = (SELECT T2.WarehouseID FROM SalesLine T2 WHERE T2.SalesLineID = @SalesLine)
						AND T1.ProductsID = @ItemId
				) > @Qty
			)
			BEGIN
				SET @Return = 1
			END
			ELSE
			BEGIN
				SET @Return = 0
			END
			RETURN @Return;
		END
	'
	EXEC(@Func);
	PRINT('Function CanReserve[FN] created.')
END
ELSE
BEGIN	
	PRINT('Function CanReserve[FN] creation faild.')
END

IF OBJECT_ID('IsPlaceInWarehouse','FN') IS NULL
BEGIN
	SET @Func = '
	CREATE FUNCTION IsPlaceInWarehouse (
		@WerehouseId int,
		@Qty int
		)
		RETURNS INT
	AS
	BEGIN
		DECLARE @Return int,@Temp int
	
		SET @Temp = (SELECT (T1.MaxCapacity-T1.ActualCapacity) FROM Warehouse T1 WHERE T1.WarehouseID = @WerehouseId)

		IF(@Temp >= @Qty)
		BEGIN
			SET @Return = 1
		END
		ELSE
		BEGIN
			SET @Return = 0
		END

		RETURN @Return
	END
	'
	EXEC(@Func);
	PRINT('Function IsPlaceInWarehouse[FN] created.')
END
ELSE
BEGIN
	PRINT('Function IsPlaceInWarehouse[FN] creation faild.')
END

IF OBJECT_ID('SalesRaport','FN') IS NULL
BEGIN
	SET @Func = '
	CREATE FUNCTION SalesRaport(@Satus char(1))
	RETURNS @retContactInformation TABLE 
	(
    SalesID INT,
	ShipmentStatus CHAR(1),
	City VARCHAR(100),
	ZipCode VARCHAR(100),
	Street VARCHAR(100),
	LocalNumber VARCHAR(100),
	WarehouseMan VARCHAR(100),
	Value FLOAT
	)
	AS 
	BEGIN
    DECLARE @Return AS TABLE(
		SalesID INT,
		ShipmentStatus CHAR(1),
		City VARCHAR(100),
		ZipCode VARCHAR(100),
		Street VARCHAR(100),
		LocalNumber VARCHAR(100),
		WarehouseManID VARCHAR(100),
		Value FLOAT
		)

	INSERT INTO @retContactInformation 
		SELECT T1.SalesID,T1.ShipmentStatus,T1.City,T1.ZipCode,T1.Street,T1.LocalNumber,T2.Surname,
			(SELECT SUM(T3.Value)
				FROM SalesLine T3
				WHERE T3.SalesLineID = ANY(SELECT T4.SalesLineID
					FROM SalesTrans T4
					WHERE T4.SalesID = T1.SalesID)
				GROUP BY T3.Value
			)
		FROM Sales T1
			INNER JOIN WarehouseMan T2
			ON T2.WarehouseManID = T1.WarehouseManID
		WHERE T1.ShipmentStatus = @Satus

	RETURN;
	END;
	'
	EXEC(@Func);
	PRINT('Function SalesRaport[FN] created.')
END
ELSE
BEGIN
	PRINT('Function SalesRaport[FN] creation faild.')
END

