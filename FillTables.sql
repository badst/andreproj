
INSERT INTO Warehouse (MaxCapacity, ActualCapacity, City, ZipCode, Street, LocalNumber)
VALUES (10,2,'Poznan','64-870','Piekna',20),
	(4,0,'Krakow','83-400','Chrobrego',3),
	(20,18,'Warszawa','34-425','Piekna',176);

INSERT INTO Products (Name, UnitPrice)
VALUES ('MP3',10),
	('MP4',99.99),
	('MP5',400),
	('9mm 50szt',35);

INSERT INTO WarehouseInvent ( Qty, WarehouseID, ProductsID, CreationUser, CreationDate, ModificationUser, ModificationDate)
VALUES (2,1,1,NULL,NULL,NULL,NULL),
	(2,2,2,NULL,NULL,NULL,NULL),
	(2,3,3,NULL,NULL,NULL,NULL),
	(2,3,4,NULL,NULL,NULL,NULL);

INSERT INTO SalesLine(Qty, WarehouseID, ProductsID)
VALUES (1,1,1),
	(2,2,2),
	(2,3,3),
	(1,2,3),
	(1,3,3);

INSERT INTO WarehouseMan (Name, Surname, BirthDate, Wage, BossID, WarehouseID)
VALUES	('Adam','Kowalski',Cast('1973-01-01' as date),2000,NULL,1),
		('Jan','Nowocki',Cast('1979-08-21' as date),2000,1,2),
		('Kamil','Tuczynski',Cast('1976-11-11' as date),3400,1,3),
		('Kuba','Tutaj',Cast('1984-04-25' as date),3000,3,3),
		('Bartek','Nieurosl',Cast('1981-02-17' as date),6700,3,3);

INSERT INTO Sales (City, ZipCode, Street, LocalNumber, WarehouseManID)
VALUES ('Wroclawek','42-630','Brazowa','12b',2),
	('Sieratowo','82-638','Owocowa','1',2),
	('Sieratowo','82-638','Mieszana','11',1),
	('Wroclawek','63-633','Osnowa','163',1),
	('Olsztyn','36-839','Grudzinska','14',1);

INSERT INTO SalesTrans(SalesLineID, SalesID)
VALUES (1,1),
	(2,2),
	(3,2),
	(3,3),
	(4,3);
